var bodyParser=require('body-parser');
var mongoose=require('mongoose');
var methodOverride=require('method-override');
var seedDb=require('./seeds');
//flash
var flash=require('connect-flash');
//models
var User=require('./models/user');
//passport
var passport=require('passport');
var localStrategy=require('passport-local');
var passportLocalMongoose=require('passport-local-mongoose');
//routes
var commentRoutes=require('./routes/comments');
var campgroundRoutes=require('./routes/campgrounds');
var indexRoutes=require('./routes/index');
//express
var express=require('express');
var app=express();
app.use(express.static(__dirname+'/public'));
app.use(bodyParser.urlencoded({extended:true}));
app.use(methodOverride('_method'));
//passport config
app.use(require('express-session')({
	secret: "this si dartvader",
	resave:false,
	saveUninitialized: false
}));
app.use(passport.initialize());
app.use(passport.session());
passport.use(new localStrategy(User.authenticate()));
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());
//flash
app.use(flash());
//attached users middleware
app.use(function(req,res,next){
	res.locals.currentUser=req.user;
	res.locals.error=req.flash('error');
	res.locals.success=req.flash('success');	
	next();
});
//view engine
app.set('view engine','ejs');
//db connect
//mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost/yelpcamp');
//seeds
//seedDb();
//start server
app.use('/',indexRoutes);
app.use('/campgrounds',campgroundRoutes);
app.use('/campgrounds/:id/comments',commentRoutes);
app.listen(6060,function(){
	console.log('Yelpcamp Server');
});

