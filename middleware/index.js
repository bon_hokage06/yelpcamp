var Campground=require('../models/campground');
var Comment=require('../models/comment');
var middlewareObject={};

middlewareObject.isLoggedIn=function(req,res,next){
		if(req.isAuthenticated()){
			next();
		}
		else{
			req.flash('error','You need to be logged in to do that!');
			res.redirect('/login');		
		}
}	
middlewareObject.checkCampgroundOwnership=function(req,res,next){
	if(req.isAuthenticated()){
		Campground.findById(req.params.id,function(err,Campground){
			if(err){
				req.flash('error','Campground not found!');
				res.redirect('back');
			}
			else{
				if(Campground.author.id.equals(req.user._id)){
					next();
				}
				else{
					req.flash('error','You don\'t have permission to do that!');
					res.redirect('back');					
				}
			}
		});
	}
	else{
		req.flash('error','You need to be logged in to do that!');
		res.redirect('back');		
	}	
}
middlewareObject.checkCommentOwnership=function(req,res,next){
	if(req.isAuthenticated()){
		Comment.findById(req.params.commentId,function(err,Comment){
			if(err){
				req.flash('error','Comment not found!');				
				res.redirect('back');
			}
			else{
				if(Comment.author.id.equals(req.user._id)){
					next();
				}
				else{
					req.flash('error','You don\'t have permission to do that!');					
					res.redirect('back');					
				}
			}
		});
	}
	else{
		req.flash('error','You need to be logged in to do that!');		
		res.redirect('back');		
	}	
}
module.exports=middlewareObject;