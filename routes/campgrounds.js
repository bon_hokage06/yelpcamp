var express=require('express');
var router=express.Router();
var Campground=require('../models/campground');
var middleware=require('../middleware');
//index
router.get('/',function(req,res){
	Campground.find({},function(err,allCampgrounds){
		if(err){
			console.log(err);
			req.flash('error','Something went wrong!');
			res.redirect('back')
		}
		else{
			res.render('campgrounds/index',{campgrounds: allCampgrounds});			
		}
	});

});
//create
router.post('/',middleware.isLoggedIn,function(req,res){
	var name=req.body.name;
	var image=req.body.image;	
	var price=req.body.price;
	var description=req.body.description;
	var author= {
		id: req.user._id,
		username: req.user.username
	};
	var newCampground={name : name ,price: price, image: image,description: description,author: author};
	Campground.create(newCampground,function(err,campground){
		if(err){
			req.flash('error','Something went wrong!');			
			console.log(err);
		}
		else{
			req.flash('success','Campground created!');
			res.redirect('/campgrounds');							
		}		
	});
});
//new
router.get('/new',middleware.isLoggedIn,function(req,res){
	res.render('campgrounds/new');
});
//edit
router.get('/:id/edit',middleware.checkCampgroundOwnership,function(req,res){
	Campground.findById(req.params.id).populate('comments').exec(function(err,campground){
		if(err){
			req.flash('error','Something went wrong!');			
			res.redirect('/campgrounds');
		}
		else{
			res.render('campgrounds/edit',{campground:campground});			
		}
	});
});
//save
router.put('/:id/',middleware.checkCampgroundOwnership,function(req,res){
	Campground.findByIdAndUpdate(req.params.id,req.body.campground,function(err,campground){
		if(err){
			req.flash('error','Something went wrong!');
			res.redirect('back');			
		}
		else{
			req.flash('success','Campground updated!');			
			res.redirect('/campgrounds/'+req.params.id);
		}
	});
});
//delete
router.delete('/:id/',middleware.checkCampgroundOwnership,function(req,res){
	Campground.findByIdAndRemove(req.params.id,function(err,campground){
		if(err){
			req.flash('error','Something went wrong!');
			res.redirect('back');			
		}
		else{
			req.flash('success','Campground deleted!');			
			res.redirect('/campgrounds/');
		}
	});
});
//show
router.get('/:id',function(req,res){
	Campground.findById(req.params.id).populate('comments').exec(function(err,campground){
		if(err){
			req.flash('error','Something went wrong!');
			res.redirect('back');			
		}
		else{
			res.render('campgrounds/show',{campground:campground});			
		}
	});
});
module.exports=router;