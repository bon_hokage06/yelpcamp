var express=require('express');
var router=express.Router({mergeParams:true});
var Campground=require('../models/campground');
var Comment=require('../models/comment');
var middleware=require('../middleware');
//new
router.get('/new',middleware.isLoggedIn,function(req,res){
	Campground.findById(req.params.id,function(err,campground){
		if(err){
			req.flash('error','Something went wrong!');
			res.redirect('back');			
		}
		else{
			res.render('comments/new',{campground:campground});			
		}
	});
});
//create
router.post('/',middleware.isLoggedIn,function(req,res){
	var author= {
		id: req.user._id,
		username: req.user.username
	};	
	var newComment={text: req.body.text,author: author};
	Comment.create(newComment,function(err,comment){
		if(err){
			req.flash('error','Something went wrong!');
			res.redirect('back');			
		}
		else{
			Campground.findById(req.params.id,function(err,campground){
				if(err){
					req.flash('error','Something went wrong!');
					res.redirect('/campgrounds');
				}
				else{
					campground.comments.push(comment);
					campground.save(function(err,campground){
						if(err){
							req.flash('error','Something went wrong!');						
							res.redirect('back');	
						}
						else{
							req.flash('success','Comment Created!');							
							res.redirect('/campgrounds/'+campground._id);
						}
					});						
				}
			});
		}
	});
});
//edit
router.get('/:commentId/edit',middleware.checkCommentOwnership,function(req,res){
	Comment.findById(req.params.commentId,function(err,Comment){
		if(err){
			req.flash('error','Something went wrong!');			
			res.redirect('back');
		}
		else{
			res.render('comments/edit',{campgroundId:req.params.id,comment: Comment});			
		}
	});
});
router.put('/:commentId/',middleware.checkCommentOwnership,function(req,res){
	Comment.findByIdAndUpdate(req.params.commentId,req.body.comment,function(err,comment){
		if(err){
			req.flash('error','Something went wrong!');			
			res.redirect('back');
		}
		else{
			req.flash('success','Comment updated!');			
			res.redirect('/campgrounds/'+req.params.id);
		}
	});
});
//delete
router.delete('/:commentId',middleware.checkCommentOwnership,function(req,res){
	Comment.findByIdAndRemove(req.params.commentId,function(err,Comment){
		if(err){
			req.flash('error','Something went wrong!');
			res.redirect('back');
		}
		else{
			req.flash('success','Comment deleted!');			
			res.redirect('/campgrounds/'+req.params.id);
		}
	});
});

module.exports=router;