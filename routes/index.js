var express=require('express');
var router=express.Router();
var User=require('../models/user');
var passport=require('passport');
router.get('/',function(req,res){
	res.render('landing');
});
//new
router.get('/register',function(req,res){
	res.render('users/register');
});
//create
router.post('/register',function(req,res){
	var newUser=new User({username:req.body.username});
	User.register(newUser,req.body.password,function(err,user){
		console.log(user);
		if(err){
			req.flash('error','Something went wrong!');			
			res.redirect('/register');
		}
		else{
			passport.authenticate('local')(req,res,function(){
				req.flash('success','User created!');			
				res.redirect('/campgrounds/');
			});			
		}
	});
});
//login
router.get('/login',function(req,res){
	res.render('users/login');
});
//login post
router.post('/login',
	passport.authenticate('local',{
	successRedirect: '/campgrounds',
	failureRedirect: '/login'
	})
	,function(req,res){
	req.flash('success','Succesfully logged in!');		
});

//logout
router.get('/logout',function(req,res){
	req.logout();
	req.flash('success','You are logged out!');
	res.redirect('/campgrounds');
});
module.exports=router;